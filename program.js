const webrtc = require('./js/webrtc')
const util = require('./js/util/util')
const kademlia = require('./js/dht/kademlia')
const crypto = require('crypto')
const shasum = crypto.createHash('sha1')
const events = require('events');
const ev = new events.EventEmitter();

const socket = io.connect('http://35.196.94.146:' + '3002' + '/')

shasum.update(Math.random().toString())
let MY_ID = shasum.digest('hex')
console.log('id', MY_ID)

const kad = new kademlia.Kademlia(MY_ID)
document.getElementById('text_id').textContent = MY_ID

socket.on('connect', function () {
    socket.emit('get_offer')
    peerOffer = new webrtc.Peer('offer')
    peerAnswer = new webrtc.Peer('answer')
})

socket.on('get_offer', function (str) {
    if (str == undefined || JSON.parse(str).kid == MY_ID) {
        offerFirst()
    } else {
        answerFirst(JSON.parse(str))
    }
})

socket.on('add_answer', function (str) {
    let data = JSON.parse(str)
    if (!peerOffer.signaling && data.target == MY_ID) {
        peerOffer.connecting(data.kid)
        peerOffer.rtc.signal(JSON.parse(data.sdp))
    }
})

function offerFirst() {
    peerOffer = new webrtc.Peer('offer')

    peerOffer.rtc.on('signal', function (data) {
        let json = {
            type: 'offer',
            kid: MY_ID,
            sdp: JSON.stringify(data)
        }
        socket.emit('add_offer', JSON.stringify(json))
    })

    peerOffer.rtc.on('connect', function () {
        console.log('offer first connected', peerOffer.kid)

        peerOffer.connected()

        kad.addknode(peerOffer)
        kad.findNode(peerOffer.kid, peerOffer)
        offerFirst()
    })

    peerOffer.rtc.on('data', function (data) {
        onCommand(data)
    })
}


function answerFirst(data) {
    peerAnswer = new webrtc.Peer('answer')

    setTimeout(function () {
        if (!peerAnswer.isConnected) {
            console.log('first answer failed')
            peerAnswer.failed()
            socket.emit('get_offer')
        }
    }, 2 * 1000)

    peerAnswer.connecting(data.kid)
    peerAnswer.rtc.signal(JSON.parse(data.sdp))

    peerAnswer.rtc.on('signal', function (v) {
        let json = {
            kid: MY_ID,
            target: data.kid,
            sdp: JSON.stringify(v)
        }
        socket.emit('add_answer', JSON.stringify(json))
    })

    peerAnswer.rtc.on('connect', function () {
        console.log('answer first connected', peerAnswer.kid)
        peerAnswer.connected()
        if (!peerOffer.isConnected) offerFirst()

        kad.addknode(peerAnswer)
        kad.findNode(peerAnswer.kid, peerAnswer)
    })

    peerAnswer.rtc.on('data', function (data) {
        onCommand(data)
    })
}

function onCommand(value) {
    if (value.toString().includes('bloadcast'))
        document.getElementById('text_dc').textContent = value
    showKbuckets()
    kad.onRequest(value)
}

function showKbuckets() {
    let show = ''
    for (let index = 0; index < kad.kbuckets.length; index++) {
        const element = kad.kbuckets[index];
        if (element.length > 0) {
            show += index + ' : '
            element.some(function (v) {
                show += (v.kid + ' , ')
            })
            show += '\n'
        }
    }
    document.getElementById('text_kbuckets').textContent = show
}

kad.ev.on('addknode', function () {
    console.log('on addknode')
    showKbuckets()
})

document.getElementById('btn_dc').onclick = function () {
    kad.bloadcast(document.getElementById('edit_datachannel').value.toString())
}