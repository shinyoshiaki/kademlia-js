"use strict"

const srv = require('http').Server()
const io = require('socket.io')(srv)

const port = 3002
srv.listen(port)
console.log('signaling server started on port:' + port)

let offer = new Object()

io.on('connection', function (socket) {

  socket.on('add_offer', function (str) {
    console.log('add_offer', str)

    while (true) {
      let id = Math.random().toString()
      if (!(id in offer)) {
        offer[id] = str
        break
      }
    }
    console.log('now offer', offer)

  })

  socket.on('get_offer', function () {
    console.log('get_offer')
    if (Object.keys(offer).length > 0) {
      let key = Object.keys(offer)[0]
      socket.emit('get_offer', offer[key])
      delete offer[key]
    } else {
      console.log('blank')
      socket.emit('get_offer', undefined)
    }
  })

  socket.on('add_answer', function (msg) {
    socket.broadcast.emit('add_answer', msg)
  })

})