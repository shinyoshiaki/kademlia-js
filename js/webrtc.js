const simplePeer = require('simple-peer')
const util = require('./util/util.js')

exports.Peer = class {
    constructor(_type) {
        this.rtc
        this.kid
        this.isConnected = false
        this.isCheking = false
        this.type = _type
        switch (_type) {
            case "offer":
                console.log('offer')
                this.initOffer()
                break
            case "answer":
                console.log('answer')
                this.initAnswer()
                break
        }
    }

    initOffer() {
        this.rtc = (new simplePeer({
            initiator: true,
            config: {
                iceServers: [{
                    urls: 'stun:stun.l.google.com:19302'
                }]
            },
            trickle: false
        }))
    }
    initAnswer() {
        this.rtc = (new simplePeer({
            initiator: false,
            config: {
                iceServers: [{
                    urls: 'stun:stun.l.google.com:19302'
                }]
            },
            trickle: false
        }))
    }

    connecting(_kid) {
        this.kid = _kid
        this.isConnected = false
    }

    connected() {
        //console.log('connected', this.kid)
        this.isConnected = true
    }

    failed() {
        console.log('connectFailed', this.kid)
    }

    disconnected() {
        console.log('disconnected', this.kid)
        this.isConnected = false
    }

    send(data) {
        try {
            console.log('webrtc send', this.kid)
            this.rtc.send(data)
        } catch (error) {
            this.disconnected()
        }
    }
}