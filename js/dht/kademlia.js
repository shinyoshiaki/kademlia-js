const webrtc = require('../webrtc')
const util = require('../util/util')
const events = require('events')
const func = require('./func')

exports.Kademlia = class {
    constructor(_kid) {
        this.k = 3
        this.kid = _kid
        this.dataList = []
        this.keyValueList = []
        this.ref = []
        this.ev = new events.EventEmitter()
        this.pingResult = []

        this.kbuckets = new Array(160)
        for (let i = 0; i < 160; i++) {
            let kbucket = []
            this.kbuckets[i] = kbucket
        }
    }

    async ping(peer) {
        let packet = JSON.stringify({
            order: 'ping',
            target: peer.kid,
            kid: this.kid
        })
        peer.send(packet)
        console.log('ping', packet)
        this.pingResult[peer.kid] = false
        setTimeout(() => {
            if (this.pingResult[peer.kid]) {
                console.log('ping success')
                return true
            }
            else {
                console.log('ping fail')
                return false
            }
        }, 3 * 1000);
    }

    store(_sender, _key, _value) {
        console.log('store')

        let packet = JSON.stringify({
            msgId: util.arrayRandom(this.dataList).toString(),
            kid: this.kid,
            sender: _sender,
            order: 'store',
            key: _key,
            value: _value
        })
        
        this.getCloseEstPeer(_key).send(packet)

        console.log('store done')
    }

    findNode(_targetID, peer) {
        console.log('findnode', _targetID)
        let packet = JSON.stringify({
            msgId: util.arrayRandom(this.dataList).toString(),
            kid: this.kid,
            targetID: _targetID,
            order: 'findnode'
        })
        peer.send(packet)
    }

    findValue() {

    }

    getCloseEstDist(_key) {
        let mini = 160
        const that = this
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some(function (v) {
                if (func.distance(_key, v.kid) < mini) {
                    mini = func.distance(_key, v.kid)
                }
            })
        }
        return mini
    }

    getCloseEstPeer(_key) {
        let mini = 160
        let peer
        const that = this
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some(function (v) {
                if (func.distance(_key, v.kid) < mini) {
                    mini = func.distance(_key, v.kid)
                    peer = v
                }
            })
        }
        return peer
    }

    getAllPeers() {
        let peers = []
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some(function (v) {
                peers.push(v)
            })
        }
        return peers
    }

    getCloseIDsAsJsonStr(targetID) {//Returns information for the k nodes it knows about closest to the target ID        
        let list = []
        const that = this
        this.getAllPeers().some(function (v) {
            if (v.kid != targetID) {
                if (list.length < that.k) list.push(v.kid)
                else {
                    for (let i = 0; i < list.length; i++) {
                        if (func.distance(list[i], targetID) > func.distance(v.kid, targetID)) {
                            list[i] = v.kid                            
                        }
                    }
                }
            }
        })

        return JSON.stringify(list)
    }

    addknode(peer) {
        let num = func.distance(this.kid, peer.kid)
        let kbucket = this.kbuckets[num]

        kbucket.push(peer)
        console.log('addknode kbuckets', num, kbucket, this.kbuckets)
        this.ev.emit('addknode')
    }

    onBloadcast(str) {
        this.getAllPeers().some(function (v) {
            if (v.isConnected) {
                v.send(str)
            }
        })
    }

    bloadcast(_data) {
        let packet = JSON.stringify({
            msgId: util.arrayRandom(this.dataList).toString(),
            kid: this.kid,
            order: 'bloadcast',
            data: _data.toString()
        })

        this.onBloadcast(packet)
    }

    kidExist(id) {
        let exist = false
        this.getAllPeers().some(function (v) {
            if (v.kid == id) {
                exist = true
                return 0
            }
        })
        return exist
    }

    getPeerFromKid(_kid) {
        let peer
        this.getAllPeers().some(function (v) {
            if (v.kid == _kid) {
                peer = v
                return 0
            }
        })
        return peer
    }

    onRequest(str) {
        let data = JSON.parse(str)
        if (!this.dataList.toString().includes(data.msgId)) {
            this.dataList.push(data.msgId)

            switch (data.order) {
                case 'store':
                    console.log('on store', data.kid)
                    let mine = func.distance(this.kid, data.key)
                    let close = this.getCloseEstDist(data.key)
                    if (mine > close) this.store(data.sender, data.key, data.value)

                    this.keyValueList[data.key] = data.value
                    break
                case 'findnode':
                    console.log('on findnode', data.kid)
                    let packet = JSON.stringify({
                        msgId: util.arrayRandom(this.dataList).toString(),
                        kid: this.kid,
                        closeIDs: this.getCloseIDsAsJsonStr(data.kid),
                        order: 'findnode-r'
                    })
                    this.getPeerFromKid(data.kid).send(packet)
                    break
                case 'findnode-r':
                    let ids = JSON.parse(data.closeIDs)
                    console.log('on findnode-r', ids)
                    let that = this
                    ids.some(function (v, i) {
                        const target = v
                        setTimeout(function () {
                            console.log('findnode offer', target)
                            that.ref.target = new webrtc.Peer('offer')
                            that.offer(target, that.ref)
                        }, i * 3000)
                    })
                    break
                case 'bloadcast':
                    this.onBloadcast(str)
                    break
                case 'ping':
                    if (data.target == this.kid) {
                        console.log('ping received')
                        this.getAllPeers().some(v => {
                            if (v.kid == data.kid) {
                                let packet = JSON.stringify({
                                    order: 'pong',
                                    target: data.kid,
                                    kid: this.kid
                                })
                                v.send(packet)
                            }
                        })
                    }
                    break
                case 'pong':
                    if (data.target == this.kid) {
                        console.log('pong received')
                        this.pingResult[data.kid] = true
                    }
                    break
            }
            this.maintain(data)
        }
    }



    cleanDiscon() {
        const that = this
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some(function (v, j) {
                if (!v.isConnected) {
                    that.kbuckets[i].splice(j, 1)
                    console.log('kbuckets cleaned', that.kbuckets)
                    return 0
                }
            })
        }
    }

    maintain(data) {
        console.log('mainatin')

        this.cleanDiscon()

        if (data.kid != this.kid) {
            let inx = func.distance(this.kid, data.kid)
            let kbucket = this.kbuckets[inx]
            if (this.kidExist(data.kid)) {
                console.log('@maintain','kid exist kbucket', inx)
                const that = this
                kbucket.some(function (v, j) {
                    if (v.kid == data.kid) {
                        console.log('@maintain','Moves it to the tail of the list')
                        let peer = v
                        kbucket.splice(j, 1)
                        that.addknode(peer)
                        return 0
                    }
                })
            } else {
                if (kbucket.length < this.k) {
                    console.log('@maintain','bucket has fewer than k entries')

                    const target = data.kid
                    this.ref.target = new webrtc.Peer('offer')
                    this.offer(target, this.ref)
                } else {
                    console.log('@maintain','bucket has mutch more than k entries')
                    this.ping(kbucket[0]).then(success => {
                        let peer = kbucket[0]
                        kbucket.splice(0, 1)
                        if (success) {
                            console.log('@maintain','it is moved to the tail of the list, and the new sender’s contact is discarded.')
                            this.addknode(peer)
                        } else {
                            console.log('@maintain','it is evicted from the k­bucket and the new sender inserted at the tail.')

                            const target = data.kid
                            this.ref.target = new webrtc.Peer('offer')
                            this.offer(target, this.ref)
                        }
                    })
                }
            }
        }

        switch (data.order) {
            case 'store':
                if (data.key == this.kid) {
                    if (data.value.includes('sdp')) {
                        if (data.value.includes('offer')) {
                            console.log('kad received offer', data.sender)

                            const target = data.sender
                            this.ref.target = new webrtc.Peer('answer')
                            this.answer(target, data.value, this.ref)
                        } else if (data.value.includes('answer')) {
                            console.log('kad received answer', data.sender)

                            const target = data.sender
                            this.ref.target.rtc.signal(JSON.parse(data.value))
                        }
                    }
                }
                break
        }

        console.log('maintain done')
    }

    offer(target, r) {
        const that = this

        r.target.connecting(target)

        console.log('kad offer', r.target.kid)

        r.target.rtc.on('error', function (err) { console.log('error', target, err) })

        r.target.rtc.on('signal', function (sdp) {
            that.store(that.kid, target, JSON.stringify(sdp))
        })

        r.target.rtc.on('connect', function () {
            console.log('kad offer connected', r.target.kid)
            r.target.connected()
            that.addknode(r.target)
        })

        r.target.rtc.on('data', function (data) {
            console.log('kad rtc recieved', r.target.kid, data.toString())
            that.onRequest(data)
        })
    }

    answer(target, sdpStr, r) {
        const that = this

        r.target.connecting(target)
        console.log('kad answer', r.target.kid)

        r.target.rtc.on('error', function (err) { console.log('error', target, err) })

        r.target.rtc.signal(JSON.parse(sdpStr))

        r.target.rtc.on('signal', function (sdp) {
            that.store(that.kid, target, JSON.stringify(sdp))
        })

        r.target.rtc.on('connect', function () {
            console.log('kad answer connected', r.target.kid)
            r.target.connected()
            that.addknode(r.target)
        })

        r.target.rtc.on('data', function (data) {
            console.log('kad rtc recieved', r.target.kid, data.toString())
            that.onRequest(data)
        })
    }
}