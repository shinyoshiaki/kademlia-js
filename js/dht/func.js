const util = require('../util/util')
const converter = new util.Converter()

exports.distance = function (a16, b16) {
    let a = converter.convertBase(a16, 16, 2).toString().split('')
    let b = converter.convertBase(b16, 16, 2).toString().split('')

    let xor
    if (a.length > b.length) xor = new Array(a.length)
    else xor = new Array(b.length)

    for (let i = 0; i < xor.length; i++) {
        xor[i] = parseInt(a[i]) ^ parseInt(b[i])
    }
    let xored = xor.toString().replace(/,/g, '')
    let n10 = parseInt(converter.convertBase(xored, 2, 10).toString())

    let n, i
    for (i = 0; ; i++) {
        n = 2 ** i
        if (n > n10) break
    }

    return i
}